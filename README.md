# Ontoflow Benchmark

This is a collection of ontologies for testing and benchmarking [Ontoflow](https://gitlab.com/kupferdigital/ontoflow).

## Installation

- Install [riot](https://jena.apache.org/documentation/io/)

## Usage

It is assumed that there is one directory per ontology.

```bash
./run-for-all.sh DIRECTORY
```
