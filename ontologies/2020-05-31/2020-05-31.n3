@prefix owl: <http://www.w3.org/2002/07/owl#> .
@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> .
@prefix dc: <http://purl.org/dc/terms/> .
@prefix ns0: <http://purl.org/vocab/vann/> .
@prefix ns1: <https://w3id.org/vocab/olca#> .
@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix foaf: <http://xmlns.com/foaf/0.1/> .
@prefix schema: <http://schema.org/> .
@prefix skos: <http://www.w3.org/2004/02/skos/core#> .

<https://w3id.org/vocab/olca>
  a owl:Ontology, <http://purl.org/vocommons/voaf#Vocabulary> ;
  rdfs:label "Ontology Loose Coupling Annotation"@en ;
  dc:title "Ontology Loose Coupling Annotation"@en ;
  rdfs:comment "A vocabulary defining annotations enabling loose coupling between classes and properties in ontologies. Those annotations define with some accuracy the expected use of properties, in particular across vocabularies, without the formal constraints entailed by the use of OWL or RDFS constructions"@en ;
  ns0:preferredNamespacePrefix "olca" ;
  ns0:preferredNamespaceUri <https://w3id.org/vocab/olca#> ;
  dc:creator <http://data.semanticweb.org/person/bernard-vatant> ;
  dc:contributor <http://data.semanticweb.org/person/carlos-tejo-alonso>, <https://w3id.org/people/gatemezing> ;
  dc:issued "2013-02-07" ;
  dc:modified "2020-02-31" ;
  owl:versionIRI <https://w3id.org/vocab/olca_v2.0.ttl> ;
  owl:versionInfo "v2.0" ;
  rdfs:seeAlso <http://www.w3.org/wiki/WebSchemas/SchemaDotOrgMetaSchema> .

<https://w3id.org/vocab/olca#similarClass>
  a owl:AnnotationProperty ;
  rdfs:label "similar class"@en ;
  rdfs:comment "A loose similarity of classes, typically across ontologies . This annotation is to be used when one does not want to enforce formally the coupling by owl:equivalentClass. "@en ;
  ns1:domainIncludes rdfs:Class, owl:Class ;
  ns1:rangeIncludes rdfs:Class, owl:Class .

ns1:similarProperty
  a owl:AnnotationProperty ;
  rdfs:label "similar property"@en ;
  rdfs:comment "A loose similary of properties, typically across ontologies . This annotation is to be used when one does not want to enforce formally the coupling by owl:equivalentProperty. "@en ;
  ns1:domainIncludes rdf:Property, owl:ObjectProperty, owl:DatatypeProperty, owl:AnnotationProperty ;
  ns1:rangeIncludes rdf:Property, owl:ObjectProperty, owl:DatatypeProperty, owl:AnnotationProperty .

ns1:domainIncludes
  a owl:AnnotationProperty ;
  rdfs:label "domain includes"@en ;
  rdfs:comment "A loose coupling of a property to possible or expected class it can describe. This annotation is to be used when one does not want to enforce formally the coupling by rdfs:domain or some owl:Restriction constraint. "@en ;
  ns1:domainIncludes rdf:Property, owl:ObjectProperty, owl:DatatypeProperty, owl:AnnotationProperty ;
  ns1:rangeIncludes rdfs:Class, owl:Class .

ns1:rangeIncludes
  a owl:AnnotationProperty ;
  rdfs:label "range includes"@en ;
  rdfs:comment "A loose coupling of a property to possible or expected values. This annotation is to be used when one does not want to enforce formally the coupling by rdfs:range or some owl:Restriction constraint."@en ;
  ns1:domainIncludes rdf:Property, owl:ObjectProperty, owl:DatatypeProperty, owl:AnnotationProperty ;
  ns1:rangeIncludes rdfs:Class, owl:Class .

ns1:expectedProperty
  a owl:AnnotationProperty ;
  rdfs:label "expected property"@en ;
  rdfs:comment "A loose coupling of a class to possible or expected properties. This annotation is to be used when one does not want to enforce formally the coupling using rdfs:range or some owl:Restriction."@en ;
  ns1:domainIncludes rdfs:Class, owl:Class ;
  ns1:rangeIncludes rdf:Property, owl:ObjectProperty, owl:DatatypeProperty, owl:AnnotationProperty .

<https://w3id.org/vocab/lingvoj#officialLanguage>
  a owl:ObjectProperty ;
  rdfs:label "official language"@en ;
  rdfs:comment "An official language of a country, project, organization or event."@en ;
  rdfs:range <https://w3id.org/vocab/lingvoj#Lingvo> ;
  ns1:domainIncludes foaf:Organization, foaf:Project, <http://linkedevents.org/ontology/Event>, schema:Country .

dc:subject
  a rdf:Property ;
  ns1:domainIncludes foaf:Document ;
  ns1:rangeIncludes skos:Concept .
