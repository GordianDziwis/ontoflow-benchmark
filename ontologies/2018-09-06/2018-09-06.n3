@prefix owl: <http://www.w3.org/2002/07/owl#> .
@prefix dc11: <http://purl.org/dc/elements/1.1/> .
@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> .
@prefix xsd: <http://www.w3.org/2001/XMLSchema#> .
@prefix ns0: <https://w3id.org/usability#> .

<https://w3id.org/usability>
  a owl:Ontology ;
  dc11:creator "Danilov Nikita" ;
  dc11:description "Ontology 'Usability' created to describe and store information about interactions of user with a software user interface"@en, "Онтология \"Удобство испльзования\" предназначена для фиксации данных о взаимодействии пользователя с программной системой посредством графического пользовательского интерфейса"@ru ;
  dc11:publisher "Denis Palashevskiy" ;
  dc11:title "Usability"@en, "Удобство использования"@ru .

<https://w3id.org/usability#contains>
  a owl:ObjectProperty, owl:TransitiveProperty ;
  owl:inverseOf <https://w3id.org/usability#isContainedIn> ;
  rdfs:comment "A transitive property which determines that some subject contains/stores some object inside of it. Inverse of isContainedIn"@en, "Транзитивное свойство, которое определят, что некий объект содержит/хранит внутря себя какой-либо другой объект"@ru ;
  rdfs:label "contains"@en, "cодержит"@ru .

<https://w3id.org/usability#hasImage>
  a owl:ObjectProperty ;
  owl:inverseOf <https://w3id.org/usability#wasImaged> ;
  rdfs:domain <https://w3id.org/usability#Variation> ;
  rdfs:range <https://w3id.org/usability#Image> ;
  rdfs:comment "The property which associate some region variation with its image"@en, "Свойство которое связывает вариацию региона с его изображением"@ru ;
  rdfs:label "hasImage"@en, "имеетИзображение"@ru .

<https://w3id.org/usability#isContainedIn>
  a owl:ObjectProperty, owl:TransitiveProperty ;
  rdfs:comment "A transitive property which determines that some object is contained/stored/located inside of some subject. Inverse of contains"@en, "Транзитивное свойство, которое определяет, что некоторый объект содержится/хранится/находится внутри какого-либо другого объекта"@ru ;
  rdfs:label "isContainedIn"@en, "содержитсяВ"@ru .

<https://w3id.org/usability#performed>
  a owl:ObjectProperty ;
  owl:inverseOf <https://w3id.org/usability#wasPerformedBy> ;
  rdfs:domain <https://w3id.org/usability#User> ;
  rdfs:range <https://w3id.org/usability#Session> ;
  rdfs:comment "The property that associates a user with a session that he performed"@ru, "Связывает пользователя с сессией, которую он выполнил"@ru ;
  rdfs:label "performed"@en, "выполнил"@ru .

<https://w3id.org/usability#wasAssociatedWith>
  a owl:ObjectProperty ;
  owl:inverseOf <https://w3id.org/usability#wasInvokedIn> ;
  rdfs:comment "The property that associates command event with the specific command instance"@en, "Связывает командное событие с экземпляром команды"@ru ;
  rdfs:label "wasAssociatedWith"@en, "связанаС"@ru .

<https://w3id.org/usability#wasImaged>
  a owl:ObjectProperty ;
  rdfs:domain <https://w3id.org/usability#Image> ;
  rdfs:range <https://w3id.org/usability#Variation> ;
  rdfs:comment "The property that associates an image with the corresponding region variation"@en, "Связывает изображение с соответствующей вариацией региона"@ru ;
  rdfs:label "wasImaged"@en, "изображает"@ru .

<https://w3id.org/usability#wasInvokedIn>
  a owl:ObjectProperty ;
  rdfs:comment "The property that associates specific command with the event in which the command was called"@en, "Связывает команду с командным событием, в котором команда была вызвана"@ru ;
  rdfs:label "wasInvokedIn"@en, "былоВызваноВ"@en .

<https://w3id.org/usability#wasPerformedBy>
  a owl:ObjectProperty ;
  rdfs:domain <https://w3id.org/usability#Session> ;
  rdfs:range <https://w3id.org/usability#User> ;
  rdfs:comment "The property that associates a session with the user who performed it"@en, "Связывает сессию с пользователем, который ее выполнял"@ru ;
  rdfs:label "wasPerformedBy"@en, "былаВыполненаПользователем"@ru .

<https://w3id.org/usability#wasPerformedOn>
  a owl:ObjectProperty ;
  owl:inverseOf <https://w3id.org/usability#wasUsedIn> ;
  rdfs:domain <https://w3id.org/usability#Session> ;
  rdfs:range <https://w3id.org/usability#Device> ;
  rdfs:comment "The property that associates a session with the device on which the session was run"@en, "Связывает сессию с устройством, на котором она выполнялась"@ru ;
  rdfs:label "wasPerformedOn"@en, "былаВыполненаНа"@ru .

<https://w3id.org/usability#wasUsedIn>
  a owl:ObjectProperty ;
  rdfs:domain <https://w3id.org/usability#Device> ;
  rdfs:range <https://w3id.org/usability#Session> ;
  rdfs:comment "The property that associates a device with the sessions which were run on that device"@en, "Связывает устройство с сессиями, которые были на нем запущены"@ru ;
  rdfs:label "wasUsedIn"@en, "былоИспользованоВ"@ru .

<https://w3id.org/usability#hasBinaryData>
  a owl:DatatypeProperty ;
  rdfs:range xsd:base64Binary .

<https://w3id.org/usability#hasCommandName>
  a owl:DatatypeProperty ;
  rdfs:domain <https://w3id.org/usability#CommandEvent> ;
  rdfs:range [
    a rdfs:Datatype ;
    owl:onDatatype xsd:string ;
    owl:withRestrictions ( _:genid2 )
  ] .

<https://w3id.org/usability#hasDateTime>
  a owl:DatatypeProperty ;
  rdfs:domain <https://w3id.org/usability#Event> ;
  rdfs:range xsd:dateTimeStamp .

<https://w3id.org/usability#hasDpiX>
  a owl:DatatypeProperty ;
  rdfs:domain <https://w3id.org/usability#Image> ;
  rdfs:range [
    a rdfs:Datatype ;
    owl:onDatatype xsd:int ;
    owl:withRestrictions ( _:genid5 )
  ] .

<https://w3id.org/usability#hasDpiY>
  a owl:DatatypeProperty ;
  rdfs:domain <https://w3id.org/usability#Image> ;
  rdfs:range [
    a rdfs:Datatype ;
    owl:onDatatype xsd:int ;
    owl:withRestrictions ( _:genid8 )
  ] .

<https://w3id.org/usability#hasEndDateTime>
  a owl:DatatypeProperty ;
  rdfs:domain <https://w3id.org/usability#Session> ;
  rdfs:range xsd:dateTimeStamp .

<https://w3id.org/usability#hasHeight>
  a owl:DatatypeProperty ;
  rdfs:domain <https://w3id.org/usability#Image> ;
  rdfs:range [
    a rdfs:Datatype ;
    owl:onDatatype xsd:double ;
    owl:withRestrictions ( _:genid11 )
  ] .

<https://w3id.org/usability#hasInRegionX>
  a owl:DatatypeProperty ;
  rdfs:range [
    a rdfs:Datatype ;
    owl:onDatatype xsd:double ;
    owl:withRestrictions ( _:genid14 )
  ] .

<https://w3id.org/usability#hasInRegionY>
  a owl:DatatypeProperty ;
  rdfs:range [
    a rdfs:Datatype ;
    owl:onDatatype xsd:double ;
    owl:withRestrictions ( _:genid17 )
  ] .

<https://w3id.org/usability#hasMaxHeight>
  a owl:DatatypeProperty ;
  rdfs:domain <https://w3id.org/usability#Variation> ;
  rdfs:range [
    a rdfs:Datatype ;
    owl:onDatatype xsd:double ;
    owl:withRestrictions ( _:genid20 )
  ] .

<https://w3id.org/usability#hasMaxWidth>
  a owl:DatatypeProperty ;
  rdfs:domain <https://w3id.org/usability#Variation> ;
  rdfs:range [
    a rdfs:Datatype ;
    owl:onDatatype xsd:double ;
    owl:withRestrictions ( _:genid23 )
  ] .

<https://w3id.org/usability#hasMinHeight>
  a owl:DatatypeProperty ;
  rdfs:domain <https://w3id.org/usability#Variation> ;
  rdfs:range [
    a rdfs:Datatype ;
    owl:onDatatype xsd:double ;
    owl:withRestrictions ( _:genid26 )
  ] .

<https://w3id.org/usability#hasMinWidth>
  a owl:DatatypeProperty ;
  rdfs:domain <https://w3id.org/usability#Variation> ;
  rdfs:range [
    a rdfs:Datatype ;
    owl:onDatatype xsd:double ;
    owl:withRestrictions ( _:genid29 )
  ] .

<https://w3id.org/usability#hasName>
  a owl:DatatypeProperty ;
  rdfs:range [
    a rdfs:Datatype ;
    owl:onDatatype xsd:string ;
    owl:withRestrictions ( _:genid32 )
  ] .

<https://w3id.org/usability#hasStartDateTime>
  a owl:DatatypeProperty ;
  rdfs:domain <https://w3id.org/usability#Session> ;
  rdfs:range xsd:dateTimeStamp .

<https://w3id.org/usability#hasUid>
  a owl:DatatypeProperty ;
  rdfs:range [
    a rdfs:Datatype ;
    owl:onDatatype xsd:string ;
    owl:withRestrictions ( _:genid35 )
  ] .

<https://w3id.org/usability#hasWidth>
  a owl:DatatypeProperty ;
  rdfs:domain <https://w3id.org/usability#Image> ;
  rdfs:range [
    a rdfs:Datatype ;
    owl:onDatatype xsd:double ;
    owl:withRestrictions ( _:genid38 )
  ] .

<https://w3id.org/usability#ActionEvent>
  a owl:Class ;
  rdfs:subClassOf <https://w3id.org/usability#Event> ;
  rdfs:comment "The base class for events which are user actions related to user interation with the interface (clicks, mouse movements, etc.)"@en, "Базовый класс, предназначенный для событий, которые представляют собой действия пользователя, относящиеся к взаимодействию пользователя с интерфейсом (клики, перемещения мыши и др.)"@ru ;
  rdfs:label "ActionEvent"@en, "ДействиеСобытие"@ru .

<https://w3id.org/usability#ClickMouseActionEvent>
  a owl:Class ;
  rdfs:subClassOf <https://w3id.org/usability#MouseActionEvent>, [
    a owl:Restriction ;
    owl:onProperty <https://w3id.org/usability#hasInRegionX> ;
    owl:qualifiedCardinality "1"^^xsd:nonNegativeInteger ;
    owl:onDataRange [
      a rdfs:Datatype ;
      owl:onDatatype xsd:double ;
      owl:withRestrictions ( _:genid42 )
    ]
  ], [
    a owl:Restriction ;
    owl:onProperty <https://w3id.org/usability#hasInRegionY> ;
    owl:qualifiedCardinality "1"^^xsd:nonNegativeInteger ;
    owl:onDataRange [
      a rdfs:Datatype ;
      owl:onDatatype xsd:double ;
      owl:withRestrictions ( _:genid46 )
    ]
  ] ;
  rdfs:comment "The class that represents any mouse click"@en, "Клик (click) — щелчок по кнопке мыши."@ru ;
  rdfs:label "ClickMouseActionEvent"@en, "КликМышьДействиеСобытие"@ru .

<https://w3id.org/usability#Command>
  a owl:Class ;
  rdfs:subClassOf [
    a owl:Restriction ;
    owl:onProperty <https://w3id.org/usability#isContainedIn> ;
    owl:someValuesFrom <https://w3id.org/usability#CommandEvent>
  ], [
    a owl:Restriction ;
    owl:onProperty <https://w3id.org/usability#wasInvokedIn> ;
    owl:someValuesFrom <https://w3id.org/usability#CommandEvent>
  ], [
    a owl:Restriction ;
    owl:onProperty <https://w3id.org/usability#hasName> ;
    owl:qualifiedCardinality "1"^^xsd:nonNegativeInteger ;
    owl:onDataRange [
      a rdfs:Datatype ;
      owl:onDatatype xsd:string ;
      owl:withRestrictions ( _:genid52 )
    ]
  ] ;
  rdfs:comment "Базовый класс команды, вызываемой пользователем."@ru ;
  rdfs:label "Command"@en, "The base class for the command called by user"@en, "Команда"@ru .

<https://w3id.org/usability#CommandEvent>
  a owl:Class ;
  rdfs:subClassOf <https://w3id.org/usability#Event>, [
    a owl:Restriction ;
    owl:onProperty <https://w3id.org/usability#contains> ;
    owl:someValuesFrom <https://w3id.org/usability#Command>
  ], [
    a owl:Restriction ;
    owl:onProperty <https://w3id.org/usability#wasAssociatedWith> ;
    owl:qualifiedCardinality "1"^^xsd:nonNegativeInteger ;
    owl:onClass <https://w3id.org/usability#Command>
  ] ;
  rdfs:comment "The class that represents events, related with the fact of calling a certain command (function) by the user"@en, "Класс, который описывает события команд. Они служат для фиксации факта вызова определенной команды (функции) пользователем."@ru ;
  rdfs:label "CommandEvent"@en, "КомандаСобытие"@ru .

<https://w3id.org/usability#Device>
  a owl:Class ;
  rdfs:subClassOf [
    a owl:Restriction ;
    owl:onProperty <https://w3id.org/usability#wasUsedIn> ;
    owl:someValuesFrom <https://w3id.org/usability#Session>
  ], [
    a owl:Restriction ;
    owl:onProperty <https://w3id.org/usability#hasName> ;
    owl:maxQualifiedCardinality "1"^^xsd:nonNegativeInteger ;
    owl:onDataRange [
      a rdfs:Datatype ;
      owl:onDatatype xsd:string ;
      owl:withRestrictions ( _:genid59 )
    ]
  ], [
    a owl:Restriction ;
    owl:onProperty <https://w3id.org/usability#hasUid> ;
    owl:maxQualifiedCardinality "1"^^xsd:nonNegativeInteger ;
    owl:onDataRange [
      a rdfs:Datatype ;
      owl:onDatatype xsd:string ;
      owl:withRestrictions ( _:genid63 )
    ]
  ] ;
  rdfs:comment "Устройство, на котором выполнялись(ются) сессии."@ru ;
  rdfs:label "Device"@en, "The device on which the sessions were performed."@en, "Устройство"@ru .

<https://w3id.org/usability#DoubleClickMouseEvent>
  a owl:Class ;
  rdfs:subClassOf <https://w3id.org/usability#ClickMouseActionEvent> ;
  rdfs:comment "The class that represents a double mouse click"@en, "Дабл-клик, двойной клик (double click) — два быстрых щелчка по кнопке мыши."@ru ;
  rdfs:label "DoubleClickMouseEvent"@en, "ДвойнойКликМышьДействиеСобытие"@ru .

<https://w3id.org/usability#DoubleTapTouchActionEvent>
  a owl:Class ;
  rdfs:subClassOf <https://w3id.org/usability#TapTouchActionEvent> ;
  rdfs:comment "The class that represents double tap - two short taps on the screen"@en, "Дабл-тап, дабл-тэп (double tap) — два коротких нажатия пальцем, сродни дабл-клику."@ru ;
  rdfs:label "DoubleTapTouchActionEvent"@en .

<https://w3id.org/usability#Event>
  a owl:Class ;
  rdfs:subClassOf [
    a owl:Restriction ;
    owl:onProperty <https://w3id.org/usability#isContainedIn> ;
    owl:someValuesFrom <https://w3id.org/usability#Variation>
  ], [
    a owl:Restriction ;
    owl:onProperty <https://w3id.org/usability#hasDateTime> ;
    owl:qualifiedCardinality "1"^^xsd:nonNegativeInteger ;
    owl:onDataRange xsd:dateTimeStamp
  ] ;
  rdfs:comment "The base class for all different kinds of event that may be occured while the user is interacting with the software"@en, "Базовый класс для событий, которые могут происходить при взаимодействии пользователя с программной системой"@ru ;
  rdfs:label "Event"@en, "Событие"@ru .

<https://w3id.org/usability#HoldTapTouchActionEvent>
  a owl:Class ;
  rdfs:subClassOf <https://w3id.org/usability#TapTouchActionEvent> ;
  rdfs:comment "The class that represents touch-and-hold action - continuous touch with a single finger"@en, "Тач-энд-холд (touch and hold) — нажать и держать."@ru ;
  rdfs:label "HoldTapTouchActionEvent"@en .

<https://w3id.org/usability#Image>
  a owl:Class ;
  rdfs:subClassOf [
    a owl:Restriction ;
    owl:onProperty <https://w3id.org/usability#hasBinaryData> ;
    owl:someValuesFrom xsd:base64Binary
  ], [
    a owl:Restriction ;
    owl:onProperty <https://w3id.org/usability#hasDpiX> ;
    owl:qualifiedCardinality "1"^^xsd:nonNegativeInteger ;
    owl:onDataRange [
      a rdfs:Datatype ;
      owl:onDatatype xsd:int ;
      owl:withRestrictions ( _:genid70 )
    ]
  ], [
    a owl:Restriction ;
    owl:onProperty <https://w3id.org/usability#hasDpiY> ;
    owl:qualifiedCardinality "1"^^xsd:nonNegativeInteger ;
    owl:onDataRange [
      a rdfs:Datatype ;
      owl:onDatatype xsd:int ;
      owl:withRestrictions ( _:genid74 )
    ]
  ], [
    a owl:Restriction ;
    owl:onProperty <https://w3id.org/usability#hasHeight> ;
    owl:qualifiedCardinality "1"^^xsd:nonNegativeInteger ;
    owl:onDataRange [
      a rdfs:Datatype ;
      owl:onDatatype xsd:double ;
      owl:withRestrictions ( _:genid78 )
    ]
  ], [
    a owl:Restriction ;
    owl:onProperty <https://w3id.org/usability#hasWidth> ;
    owl:qualifiedCardinality "1"^^xsd:nonNegativeInteger ;
    owl:onDataRange [
      a rdfs:Datatype ;
      owl:onDatatype xsd:double ;
      owl:withRestrictions ( _:genid82 )
    ]
  ] ;
  rdfs:comment "Class that describes an image of the region variation"@ru, "Класс, описывающий изображение, хранящее внешний вид вариации региона"@ru ;
  rdfs:label "Image"@en, "Изображение"@ru .

<https://w3id.org/usability#KeyboardActionEvent>
  a owl:Class ;
  rdfs:subClassOf <https://w3id.org/usability#ActionEvent> ;
  rdfs:comment "The class that represents actions related to user's manipulations with a keyboard"@en, "Класс, который описывает событий действий, связанных с манипуляцией пользователя с клавиатурой"@ru ;
  rdfs:label "KeyboardActionEvent"@en, "КлавиатураДействиеСобытие"@ru .

<https://w3id.org/usability#LongTapTouchActionEvent>
  a owl:Class ;
  rdfs:subClassOf <https://w3id.org/usability#TapTouchActionEvent> ;
  rdfs:comment "The class that represents touch - a touch that is slightly longer then tap"@en, "Тач (touch) — нажатие длиннее, чем Тап."@ru ;
  rdfs:label "LongTapTouchActionEvent"@en .

<https://w3id.org/usability#MouseActionEvent>
  a owl:Class ;
  rdfs:subClassOf <https://w3id.org/usability#ActionEvent> ;
  rdfs:comment "The class that represents actions related to user's manipulations with a mouse"@en, "Класс, который описывает события действий, связанных с манипуляцией пользователя с мышью"@ru ;
  rdfs:label "MouseActionEvent"@en, "МышьДействиеСобытие"@ru .

<https://w3id.org/usability#MoveMouseActionEvent>
  a owl:Class ;
  rdfs:subClassOf <https://w3id.org/usability#MouseActionEvent>, [
    a owl:Restriction ;
    owl:onProperty <https://w3id.org/usability#hasInRegionX> ;
    owl:qualifiedCardinality "1"^^xsd:nonNegativeInteger ;
    owl:onDataRange [
      a rdfs:Datatype ;
      owl:onDatatype xsd:double ;
      owl:withRestrictions ( _:genid86 )
    ]
  ], [
    a owl:Restriction ;
    owl:onProperty <https://w3id.org/usability#hasInRegionY> ;
    owl:qualifiedCardinality "1"^^xsd:nonNegativeInteger ;
    owl:onDataRange [
      a rdfs:Datatype ;
      owl:onDatatype xsd:double ;
      owl:withRestrictions ( _:genid90 )
    ]
  ] ;
  rdfs:comment "The class that represents movements of cursor. hasInRegionX and hasInRegionY properties store information about the new cursor coordinates after the movement"@en, "Класс, который соответствует событию движения курсора мыши. Свойства hasInRegionX и hasInRegionY позволяют указать новые координаты курсора мыши после перемещения"@ru ;
  rdfs:label "MoveMouseAcitonEvent"@en, "ДвижениеМышьДействиеСобытие"@ru .

<https://w3id.org/usability#PinchTouchActionEvent>
  a owl:Class ;
  rdfs:subClassOf <https://w3id.org/usability#TouchActionEvent> ;
  rdfs:comment "The class that represents pinch - a contractive movement with two fingers on the screen to scale down the image."@en, "Пинч (pinch) — сжимающее движение одновременно двумя пальцами по экрану для уменьшения изображения."@ru ;
  rdfs:label "PinchTouchActionEvent"@en .

<https://w3id.org/usability#Region>
  a owl:Class ;
  rdfs:subClassOf [
    a owl:Restriction ;
    owl:onProperty <https://w3id.org/usability#contains> ;
    owl:someValuesFrom <https://w3id.org/usability#Variation>
  ], [
    a owl:Restriction ;
    owl:onProperty <https://w3id.org/usability#isContainedIn> ;
    owl:someValuesFrom <https://w3id.org/usability#Session>
  ], [
    a owl:Restriction ;
    owl:onProperty <https://w3id.org/usability#hasMaxHeight> ;
    owl:someValuesFrom [
      a rdfs:Datatype ;
      owl:onDatatype xsd:double ;
      owl:withRestrictions ( _:genid96 )
    ]
  ], [
    a owl:Restriction ;
    owl:onProperty <https://w3id.org/usability#hasMaxWidth> ;
    owl:someValuesFrom [
      a rdfs:Datatype ;
      owl:onDatatype xsd:double ;
      owl:withRestrictions ( _:genid100 )
    ]
  ], [
    a owl:Restriction ;
    owl:onProperty <https://w3id.org/usability#hasMinHeight> ;
    owl:someValuesFrom [
      a rdfs:Datatype ;
      owl:onDatatype xsd:double ;
      owl:withRestrictions ( _:genid104 )
    ]
  ], [
    a owl:Restriction ;
    owl:onProperty <https://w3id.org/usability#hasMinWidth> ;
    owl:someValuesFrom [
      a rdfs:Datatype ;
      owl:onDatatype xsd:double ;
      owl:withRestrictions ( _:genid108 )
    ]
  ], [
    a owl:Restriction ;
    owl:onProperty <https://w3id.org/usability#hasName> ;
    owl:qualifiedCardinality "1"^^xsd:nonNegativeInteger ;
    owl:onDataRange [
      a rdfs:Datatype ;
      owl:onDatatype xsd:string ;
      owl:withRestrictions ( _:genid112 )
    ]
  ] ;
  rdfs:comment "A fragment of GUI, for example, the whole window, or a part of it"@en, "Область пользовательского интерфейса, например, окно целиком, либо его отдельная часть"@ru ;
  rdfs:label "Region"@en, "Регион"@ru .

<https://w3id.org/usability#ScrollMouseActionEvent>
  a owl:Class ;
  rdfs:subClassOf <https://w3id.org/usability#MouseActionEvent> ;
  rdfs:comment "The class that represents a mouse wheel scroll"@en, "Класс, который соответствует событию прокрутки колесика мыши"@ru ;
  rdfs:label "ScrollMouseActionEvent"@en, "ПрокруткаМышьДействиеСобытие"@ru .

<https://w3id.org/usability#Session>
  a owl:Class ;
  rdfs:subClassOf [
    a owl:Restriction ;
    owl:onProperty <https://w3id.org/usability#contains> ;
    owl:someValuesFrom <https://w3id.org/usability#Region>
  ], [
    a owl:Restriction ;
    owl:onProperty <https://w3id.org/usability#wasPerformedBy> ;
    owl:qualifiedCardinality "1"^^xsd:nonNegativeInteger ;
    owl:onClass <https://w3id.org/usability#User>
  ], [
    a owl:Restriction ;
    owl:onProperty <https://w3id.org/usability#wasPerformedOn> ;
    owl:qualifiedCardinality "1"^^xsd:nonNegativeInteger ;
    owl:onClass <https://w3id.org/usability#Device>
  ], [
    a owl:Restriction ;
    owl:onProperty <https://w3id.org/usability#hasEndDateTime> ;
    owl:qualifiedCardinality "1"^^xsd:nonNegativeInteger ;
    owl:onDataRange xsd:dateTimeStamp
  ], [
    a owl:Restriction ;
    owl:onProperty <https://w3id.org/usability#hasStartDateTime> ;
    owl:qualifiedCardinality "1"^^xsd:nonNegativeInteger ;
    owl:onDataRange xsd:dateTimeStamp
  ], [
    a owl:Restriction ;
    owl:onProperty <https://w3id.org/usability#hasUid> ;
    owl:maxQualifiedCardinality "1"^^xsd:nonNegativeInteger ;
    owl:onDataRange [
      a rdfs:Datatype ;
      owl:onDatatype xsd:string ;
      owl:withRestrictions ( _:genid121 )
    ]
  ] ;
  rdfs:comment "A period of time when a user interacts with the software"@en, "Временной промежуток, в течение которого пользователь взаимодействует с программной системой"@ru ;
  rdfs:label "Session"@en, "Сессия"@ru .

<https://w3id.org/usability#SingleClickMouseEvent>
  a owl:Class ;
  rdfs:subClassOf <https://w3id.org/usability#ClickMouseActionEvent> ;
  rdfs:comment "The class that represents a single mouse click"@en, "Клик (click) — одинарный щелчок по кнопке мыши, короткое нажатие."@ru ;
  rdfs:label "SingleClickMouseEvent"@en, "ОдинарныйКликМышьДействиеСобытие"@ru .

<https://w3id.org/usability#SingleTapTouchActionEvent>
  a owl:Class ;
  rdfs:subClassOf <https://w3id.org/usability#TapTouchActionEvent> ;
  rdfs:comment "The class that represents tap action - single short touch with one finger"@en, "Тап, тэп (tap) — короткое нажатие пальцем, сродни клику."@ru ;
  rdfs:label "SingleTapTouchActionEvent"@en .

<https://w3id.org/usability#StretchTouchActionEvent>
  a owl:Class ;
  rdfs:subClassOf <https://w3id.org/usability#TouchActionEvent> ;
  rdfs:comment "The class that represents stretch (pinch it open) - tensile movement with two fingers on the screen to enlarge the image."@en, "Стретч (stretch: для Microsoft), Пинч-ит-опен (pinch it open: для Apple) — растягивающее движение одновременно двумя пальцами по экрану для увеличения изображения."@ru ;
  rdfs:label "StretchTouchActionEvent"@en .

<https://w3id.org/usability#SwipeTouchActionEvent>
  a owl:Class ;
  rdfs:subClassOf <https://w3id.org/usability#TouchActionEvent>, [
    a owl:Restriction ;
    owl:onProperty <https://w3id.org/usability#hasInRegionX> ;
    owl:qualifiedCardinality "1"^^xsd:nonNegativeInteger ;
    owl:onDataRange [
      a rdfs:Datatype ;
      owl:onDatatype xsd:double ;
      owl:withRestrictions ( _:genid125 )
    ]
  ], [
    a owl:Restriction ;
    owl:onProperty <https://w3id.org/usability#hasInRegionY> ;
    owl:qualifiedCardinality "1"^^xsd:nonNegativeInteger ;
    owl:onDataRange [
      a rdfs:Datatype ;
      owl:onDatatype xsd:double ;
      owl:withRestrictions ( _:genid129 )
    ]
  ] ;
  rdfs:comment "The class the represents swipe (slide) - a continuous movement of the finger across the screen"@en, "Свайп (swipe), Слайд (slide) — продолжительное скольжение пальцем по экрану."@ru ;
  rdfs:label "SwipeTouchActionEvent"@en .

<https://w3id.org/usability#TapTouchActionEvent>
  a owl:Class ;
  rdfs:subClassOf <https://w3id.org/usability#TouchActionEvent>, [
    a owl:Restriction ;
    owl:onProperty <https://w3id.org/usability#hasInRegionX> ;
    owl:qualifiedCardinality "1"^^xsd:nonNegativeInteger ;
    owl:onDataRange [
      a rdfs:Datatype ;
      owl:onDatatype xsd:double ;
      owl:withRestrictions ( _:genid133 )
    ]
  ], [
    a owl:Restriction ;
    owl:onProperty <https://w3id.org/usability#hasInRegionY> ;
    owl:qualifiedCardinality "1"^^xsd:nonNegativeInteger ;
    owl:onDataRange [
      a rdfs:Datatype ;
      owl:onDatatype xsd:double ;
      owl:withRestrictions ( _:genid137 )
    ]
  ] ;
  rdfs:comment "The class that represents differend kinds events related to user's tap on the touch screen"@en, "Класс, предназначенный для описания различных видов тапов - прикосновений пользователя к экрану одним пальцем без движения"@ru ;
  rdfs:label "TapTouchActionEvent"@en .

<https://w3id.org/usability#TouchActionEvent>
  a owl:Class ;
  rdfs:subClassOf <https://w3id.org/usability#ActionEvent> ;
  rdfs:comment "The class that represents events related to user interations with touchscreen, like smartphone or tablet PC"@en, "Класс, который описывает события действий, связанных с взаимодействием пользователя с сенсорным экраном (тачскрин, англ. touchscreen), например, дисплеем телефона или планшетного компьютера"@ru ;
  rdfs:label "TouchActionEvent"@en, "КасаниеДействиеСобытие"@ru .

<https://w3id.org/usability#TypeKeyboardActionEvent>
  a owl:Class ;
  rdfs:subClassOf <https://w3id.org/usability#KeyboardActionEvent> ;
  rdfs:comment "The class that represents clicks on a keyboard"@en, "Класс, который описывает события нажатия клавиш клавиатуры"@ru ;
  rdfs:label "TypeKeyboardActionEvent"@en, "ВводКлавиатураДействиеСобытие"@ru .

<https://w3id.org/usability#User>
  a owl:Class ;
  rdfs:subClassOf [
    a owl:Restriction ;
    owl:onProperty <https://w3id.org/usability#performed> ;
    owl:someValuesFrom <https://w3id.org/usability#Session>
  ], [
    a owl:Restriction ;
    owl:onProperty <https://w3id.org/usability#hasName> ;
    owl:maxQualifiedCardinality "1"^^xsd:nonNegativeInteger ;
    owl:onDataRange [
      a rdfs:Datatype ;
      owl:onDatatype xsd:string ;
      owl:withRestrictions ( _:genid142 )
    ]
  ], [
    a owl:Restriction ;
    owl:onProperty <https://w3id.org/usability#hasUid> ;
    owl:maxQualifiedCardinality "1"^^xsd:nonNegativeInteger ;
    owl:onDataRange [
      a rdfs:Datatype ;
      owl:onDatatype xsd:string ;
      owl:withRestrictions ( _:genid146 )
    ]
  ] ;
  rdfs:comment "The user who runs sessions"@en, "Пользователь, который выполнял(ет) сессии."@ru ;
  rdfs:label "User"@en, "Пользователь"@ru .

<https://w3id.org/usability#Variation>
  a owl:Class ;
  rdfs:subClassOf [
    a owl:Restriction ;
    owl:onProperty <https://w3id.org/usability#contains> ;
    owl:someValuesFrom <https://w3id.org/usability#Event>
  ], [
    a owl:Restriction ;
    owl:onProperty <https://w3id.org/usability#isContainedIn> ;
    owl:someValuesFrom <https://w3id.org/usability#Region>
  ], [
    a owl:Restriction ;
    owl:onProperty <https://w3id.org/usability#hasImage> ;
    owl:qualifiedCardinality "1"^^xsd:nonNegativeInteger ;
    owl:onClass <https://w3id.org/usability#Image>
  ], [
    a owl:Restriction ;
    owl:onProperty <https://w3id.org/usability#hasMaxHeight> ;
    owl:maxQualifiedCardinality "1"^^xsd:nonNegativeInteger ;
    owl:onDataRange [
      a rdfs:Datatype ;
      owl:onDatatype xsd:double ;
      owl:withRestrictions ( _:genid153 )
    ]
  ], [
    a owl:Restriction ;
    owl:onProperty <https://w3id.org/usability#hasMaxWidth> ;
    owl:maxQualifiedCardinality "1"^^xsd:nonNegativeInteger ;
    owl:onDataRange [
      a rdfs:Datatype ;
      owl:onDatatype xsd:double ;
      owl:withRestrictions ( _:genid157 )
    ]
  ], [
    a owl:Restriction ;
    owl:onProperty <https://w3id.org/usability#hasMinHeight> ;
    owl:maxQualifiedCardinality "1"^^xsd:nonNegativeInteger ;
    owl:onDataRange [
      a rdfs:Datatype ;
      owl:onDatatype xsd:double ;
      owl:withRestrictions ( _:genid161 )
    ]
  ], [
    a owl:Restriction ;
    owl:onProperty <https://w3id.org/usability#hasMinWidth> ;
    owl:maxQualifiedCardinality "1"^^xsd:nonNegativeInteger ;
    owl:onDataRange [
      a rdfs:Datatype ;
      owl:onDatatype xsd:double ;
      owl:withRestrictions ( _:genid165 )
    ]
  ], [
    a owl:Restriction ;
    owl:onProperty <https://w3id.org/usability#hasName> ;
    owl:maxQualifiedCardinality "1"^^xsd:nonNegativeInteger ;
    owl:onDataRange [
      a rdfs:Datatype ;
      owl:onDatatype xsd:string ;
      owl:withRestrictions ( _:genid169 )
    ]
  ] ;
  rdfs:comment "Class that describes a region variation, since a region may have one or more variations. Variation is a unique combination of region image's height and width, or their range"@en, "Класс предназначен для описания вариации региона, так как каждый регион, в свою очередь, может иметь одну или более вариаций. Под вариацией региона в общем случае может пониматься уникальное сочетание параметров высоты и ширины изображения региона, либо их диапазон"@ru ;
  rdfs:label "Variation"@en, "ВариацияРегиона"@ru .

<https://w3id.org/usability#CommandA>
  a owl:NamedIndividual, <https://w3id.org/usability#Command> ;
  ns0:hasName "CommandA"^^xsd:string .

ns0:CommandB
  a owl:NamedIndividual, ns0:Command ;
  ns0:isContainedIn ns0:RegionVariation211 ;
  ns0:wasInvokedIn ns0:RegionVariation211 ;
  ns0:hasName "CommandB"^^xsd:string .

ns0:CommandEvent1113
  a owl:NamedIndividual, ns0:CommandEvent ;
  ns0:contains ns0:CommandA ;
  ns0:isContainedIn ns0:RegionVariation111 ;
  ns0:wasAssociatedWith ns0:CommandA .

ns0:CommandEvent1114
  a owl:NamedIndividual, ns0:CommandEvent ;
  ns0:contains ns0:CommandA ;
  ns0:isContainedIn ns0:RegionVariation111 ;
  ns0:wasAssociatedWith ns0:CommandA .

ns0:CommandEvent2112
  a owl:NamedIndividual, ns0:CommandEvent ;
  ns0:contains ns0:CommandB ;
  ns0:wasAssociatedWith ns0:CommandB .

ns0:Device1
  a owl:NamedIndividual, ns0:Device ;
  ns0:wasUsedIn ns0:Session1, ns0:Session2 .

ns0:Event1111
  a owl:NamedIndividual, ns0:Event ;
  ns0:hasInRegionX 2.700000e+1 ;
  ns0:hasInRegionY 6.250000e+2 .

ns0:Event1112 a owl:NamedIndividual, ns0:Event .
ns0:Event1121 a owl:NamedIndividual, ns0:Event .
ns0:Event1122 a owl:NamedIndividual, ns0:Event .
ns0:Event2111 a owl:NamedIndividual, ns0:Event .
ns0:Image111 a owl:NamedIndividual, ns0:Image .
ns0:Image112 a owl:NamedIndividual, ns0:Image .
ns0:Image211 a owl:NamedIndividual, ns0:Image .
ns0:Region11
  a owl:NamedIndividual, ns0:Region ;
  ns0:contains ns0:RegionVariation111, ns0:RegionVariation112 ;
  ns0:hasName "Region11"^^xsd:string .

ns0:Region21
  a owl:NamedIndividual, ns0:Region ;
  ns0:contains ns0:RegionVariation211 ;
  ns0:hasName "Region21"^^xsd:string .

ns0:RegionVariation111
  a owl:NamedIndividual, ns0:Variation ;
  ns0:contains ns0:CommandEvent1113, ns0:CommandEvent1114, ns0:Event1111, ns0:Event1112 ;
  ns0:hasImage ns0:Image111 ;
  ns0:hasName "RegionVariation111"^^xsd:string .

ns0:RegionVariation112
  a owl:NamedIndividual, ns0:Variation ;
  ns0:hasImage ns0:Image112 .

ns0:RegionVariation211
  a owl:NamedIndividual, ns0:Variation ;
  ns0:contains ns0:CommandEvent2112 ;
  ns0:hasImage ns0:Image211 .

ns0:Session1
  a owl:NamedIndividual, ns0:Session ;
  ns0:contains ns0:Region11 .

ns0:Session2
  a owl:NamedIndividual, ns0:Session ;
  ns0:contains ns0:Region21 .

ns0:User1
  a owl:NamedIndividual, ns0:User ;
  ns0:performed ns0:Session1 ;
  ns0:hasName "Ivan Ivanov"^^xsd:string .

ns0:User2
  a owl:NamedIndividual, ns0:User ;
  ns0:performed ns0:Session2 ;
  ns0:hasName "Petr Petrov"^^xsd:string .

_:genid2 xsd:minLength "1"^^xsd:int .
_:genid5 xsd:minExclusive "0"^^xsd:int .
_:genid8 xsd:minExclusive "0"^^xsd:int .
_:genid11 xsd:minInclusive 0.000000e+0 .
_:genid14 xsd:minInclusive 0.000000e+0 .
_:genid17 xsd:minInclusive 0.000000e+0 .
_:genid20 xsd:minInclusive 0.000000e+0 .
_:genid23 xsd:minInclusive 0.000000e+0 .
_:genid26 xsd:minInclusive 0.000000e+0 .
_:genid29 xsd:minInclusive 0.000000e+0 .
_:genid32 xsd:minLength "1"^^xsd:int .
_:genid35 xsd:minLength "1"^^xsd:int .
_:genid38 xsd:minInclusive 0.000000e+0 .
_:genid42 xsd:minInclusive 0.000000e+0 .
_:genid46 xsd:minInclusive 0.000000e+0 .
_:genid52 xsd:minLength "1"^^xsd:int .
_:genid59 xsd:minLength "1"^^xsd:int .
_:genid63 xsd:minLength "1"^^xsd:int .
_:genid70 xsd:minExclusive "0"^^xsd:int .
_:genid74 xsd:minExclusive "0"^^xsd:int .
_:genid78 xsd:minInclusive 0.000000e+0 .
_:genid82 xsd:minInclusive 0.000000e+0 .
_:genid86 xsd:minInclusive 0.000000e+0 .
_:genid90 xsd:minInclusive 0.000000e+0 .
_:genid96 xsd:minInclusive 0.000000e+0 .
_:genid100 xsd:minInclusive 0.000000e+0 .
_:genid104 xsd:minInclusive 0.000000e+0 .
_:genid108 xsd:minInclusive 0.000000e+0 .
_:genid112 xsd:minLength "1"^^xsd:int .
_:genid121 xsd:minLength "1"^^xsd:int .
_:genid125 xsd:minInclusive 0.000000e+0 .
_:genid129 xsd:minInclusive 0.000000e+0 .
_:genid133 xsd:minInclusive 0.000000e+0 .
_:genid137 xsd:minInclusive 0.000000e+0 .
_:genid142 xsd:minLength "1"^^xsd:int .
_:genid146 xsd:minLength "1"^^xsd:int .
_:genid153 xsd:minInclusive 0.000000e+0 .
_:genid157 xsd:minInclusive 0.000000e+0 .
_:genid161 xsd:minInclusive 0.000000e+0 .
_:genid165 xsd:minInclusive 0.000000e+0 .
_:genid169 xsd:minLength "1"^^xsd:int .