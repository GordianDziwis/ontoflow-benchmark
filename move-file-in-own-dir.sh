#!/bin/bash

for ontology in $1/*; do
  directory=${ontology%.*}
  # echo $ontology
  # echo $directory
  mkdir -p ./$directory
  mv $ontology $directory/$(basename $ontology)
done

